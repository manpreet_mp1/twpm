var elixir = require('laravel-elixir');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    /**
     * Copy needed files from /node directories
     * to /public directory.
     */
    mix
//                .copy(
//                        'node_modules/font-awesome/fonts',
//                        'public/build/fonts/font-awesome'
//                        )
//                .copy(
//                        'node_modules/bootstrap-sass/assets/fonts/bootstrap',
//                        'public/build/fonts/bootstrap'
//                        )
//
//        /**
//         * Process frontend SCSS stylesheets
//         */
        .sass([
                'frontend/app.scss',
                'plugin/sweetalert/sweetalert.scss'
        ], 'public/css/frontend.css')
        .scripts([
                'dist/frontend.js',
                'plugin/sweetalert/sweetalert.min.js',
        ], 'public/js/frontend.js')
//
//        /**
//         * Home page SCSS stylesheets
//         */
        .sass([
                'frontend/pages/home.scss',
                'plugin/sweetalert/sweetalert.scss'
        ], 'public/css/home.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js',
                'frontend/includes/navbar.js',
                'frontend/home.js'
        ], 'public/js/home.js')
        /**
         * Services page SCSS stylesheets
         */
        .sass([
                'frontend/pages/services.scss',
                'plugin/sweetalert/sweetalert.scss'
        ], 'public/css/services.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'plugin/underscore.min.js',
                'plugin/jquery-ui.js',
                'frontend/common.js',
                'frontend/services.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js'
        ], 'public/js/services.js')
//        /**
//         * About page SCSS stylesheets
//         */
        .sass([
                'plugin/animate.scss',
                'frontend/pages/about.scss'
        ], 'public/css/about.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/jquery-ui.js',
                'plugin/bootstrap.3.3.7.min.js',
                'frontend/about.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/about.js')
//        /**
//         * Blog page SCSS stylesheets
//         */
        .sass([
                'frontend/blog/blog.scss'
        ], 'public/css/blog.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'plugin/underscore.min.js',
                'plugin/moment.min.js',
                'frontend/blog.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/blog.js')
//        /**
//         * Resources page SCSS stylesheets
//         */
        .sass([
                'frontend/pages/resources.scss'
        ], 'public/css/resources.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'plugin/underscore.min.js',
                'plugin/jquery-ui.js',
                'frontend/resources.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/resources.js')
//        /**
//         * Single-blog page SCSS stylesheets
//         */
        .sass([
                'frontend/blog/single-blog.scss'
        ], 'public/css/single-blog.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'plugin/sharrre.js',
                'frontend/single-blog.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/single-blog.js')
//        /**
//         * Privacy SCSS stylesheets
//         */
        .sass([
                'frontend/pages/privacy.scss'
        ], 'public/css/privacy.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/privacy.js')
//        /**
//         * Legal SCSS stylesheets
//         */
        .sass([
                'frontend/pages/legal.scss'
        ], 'public/css/legal.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/legal.js')
        /**
         * Terms SCSS stylesheets
         */
        .sass([
                'frontend/pages/terms.scss'
        ], 'public/css/terms.css')
        .scripts([
                'plugin/jquery-3.1.1.min.js',
                'plugin/bootstrap.3.3.7.min.js',
                'frontend/includes/navbar.js',
                'plugin/sweetalert/sweetalert.min.js',
                'frontend/common.js'
        ], 'public/js/terms.js')
                    /**
         * Client Step1 SCSS stylesheets
         */
//            .sass([
//                'frontend/client/client.scss'
//            ], 'public/css/client.css')
//            //        /**
////         * Client Step1 SCSS stylesheets
////         */
//            .sass([
//                'frontend/client/dashboard.scss'
//            ], 'public/css/dashboard.css')
//            .scripts([
//                'plugin/jquery-3.1.1.min.js',
//                'plugin/bootstrap.3.3.7.min.js',
//            ], 'public/js/dashboard.js')






        /**
         * Process backend SCSS stylesheets
         */
        .sass([
                'backend/app.scss',
                'plugin/toastr/toastr.scss',
                'plugin/sweetalert/sweetalert.scss'
        ], 'public/css/backend.css')
        .scripts([
                'dist/backend.js',
                'plugin/sweetalert/sweetalert.min.js',
                'plugin/toastr/toastr.min.js',
//                'plugins.js',
                'backend/custom.js'
        ], 'public/js/backend.js')

            /**
             * Apply version control
             */
            .version([
                "public/css/home.css",
                "public/js/home.js",
                "public/css/services.css",
                "public/js/services.js",
                "public/css/about.css",
                "public/js/about.js",
                "public/css/resources.css",
                "public/js/resources.js",
                "public/css/blog.css",
                "public/js/blog.js",
                "public/css/single-blog.css",
                "public/js/single-blog.js",
                "public/css/privacy.css",
                "public/js/privacy.js",
                "public/css/legal.css",
                "public/js/legal.js",
                "public/css/terms.css",
                "public/js/terms.js",
                "public/css/backend.css",
                "public/js/backend.js",
                "public/css/client.css",
                "public/css/dashboard.css",
            ]);
});