<?php

namespace App\Models\Access\User;

use Illuminate\Notifications\Notifiable;
use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\UserSendPasswordReset;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;

/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable {

    use UserScope,
        UserAccess,
        Notifiable,
        SoftDeletes,
        UserAttribute,
        UserRelationship,
        UserSendPasswordReset;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'confirmation_code', 'confirmed'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->table = config('access.users_table');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    /**
     * Returns Requested User Deatils
     * @param type $user_id
     * @return type array
     */
    public static function userData($user_id)
    {
        $fields = ['id', 'email'];
        $profileFields = ['id', 'user_id', 'sex', 'first_name', 'middle_name', 'last_name', 'first_address', 'last_address', 'city', 'state',
            'zip', 'phone', 'phone_type', 'phone_verified', 'dob', 'ssn'];
        $userData = User::with(['profile' => function($q) use ($profileFields) {
                        $q->select($profileFields);
                    }])->where('id', $user_id)->first($fields);

        if (!$userData) {
            abort(404, 'Not Found.');
        }
        return $userData;
    }

}
