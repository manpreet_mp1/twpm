<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SaveFooterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'heading_subfooter' => 'required|max:255',
            'phone' => 'required|max:255',
            'footer_text' => 'required',
            'navbar2_text' => 'required',
            'navbar2_link' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'heading_subfooter.required' => 'Subfooter Heading is required',
            'phone.required' => 'Phone Number is required',
            'footer_text.required' => 'Footer Text is required',
            'navbar2_text.required' => 'Navbar Text is required',
            'navbar2_link.required' => 'Navbar Link is required'
        ]; 
    }

}
