<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\ServiceCategory;
use App\Models\Backend\Service;
use App\Models\Backend\ServiceServiceCategory;
use App\Models\Backend\ServiceFile;
use Redirect;
use App\Http\Requests\Backend\SaveServiceCategoryRequest;
use App\Http\Requests\Backend\SaveServiceRequest;

class ServicesController extends Controller
{
    /* SERVICES */

    public function index()
    {
        $services = Service::orderBy('id', 'desc')->get();
        return view('backend.services.index', compact('services'));
    }

    public function createService()
    {
        $serviceCategories = ServiceCategory::get();
        return view('backend.services.create', compact('serviceCategories'));
    }

    public function saveService(SaveServiceRequest $request)
    {
        $files = $request->file();
        $input = $request->input();

        $image = $this->_uploadImages($files['image'], 'img/backend/services');
        $service = Service::create([
                    'title' => $input['title'],
                    'title_bg_color' => ($input['title_bg_color']) ? $input['title_bg_color'] : '#002d62',
                    'order' => $input['order'],
                    'image' => $image,
                    'description' => $input['description'],
                    'recommended_for' => $input['recommended_for'],
                    'cost' => $input['cost'],
                    'fee_schedule' => $input['fee_schedule']
        ]);
        $service_id = $service->id;

        foreach ($input['service_category_id'] as $serviceCategoryId) {
            $service->serviceCategories()->attach(['service_category_id' => $serviceCategoryId]);
        }

        $this->_saveUploadedFiles($request, $service_id);
        return redirect()->route('admin.services')->withFlashSuccess('Service successfully created.');
    }

    private function _uploadImages($file, $directory)
    {
        $fileName = date('d-m-y-h-i-s-') . $file->getClientOriginalName();
        $file->move($directory, $fileName);
        return $fileName;
    }
    
    private function _saveUploadedFiles($request, $service_id)
    {
        $fileArray = [];
        $files = $request->file();
        $input = $request->input();
        $serviceFile = ServiceFile::where('service_id', $service_id);
        $existingServiceFiles = $serviceFile->get()->count();

        if (isset($files['upload_file'])) {
            $fileArray = $this->_createFileArray($input, $files);
            if ($existingServiceFiles) {
                $serviceFile->delete();
            }
            $this->_createServiceFile($fileArray, $service_id);
        } else {
            if (!empty($input['upload_file_old'])) {
                $fileArray = $this->_updateFileArray($input);
                $serviceFile->delete();
                $this->_createServiceFile($fileArray, $service_id);
            } else {
                $serviceFile->delete();
            }
        }
    }

    private function _createFileArray($input, $files)
    {
        foreach ($input['file_text'] as $key => $fileText) {
            $fileArray[$key]['file_text'] = $fileText;
            if (isset($files['upload_file'][$key])) {
                $fileArray[$key]['upload_file'] = $this->_uploadImages($files['upload_file'][$key], 'img/backend/services/file_uploads');
            } else {
                $fileArray[$key]['upload_file'] = $input['upload_file_old'][$key];
            }
        }
        return $fileArray;
    }

    private function _updateFileArray($input)
    {
        foreach ($input['file_text'] as $key => $fileText) {
            $fileArray[$key]['file_text'] = $fileText;
            $fileArray[$key]['upload_file'] = $input['upload_file_old'][$key];
        }
        return $fileArray;
    }

    private function _createServiceFile($fileArray, $service_id)
    {
        foreach ($fileArray as $serviceFile) {
            ServiceFile::create([
                'service_id' => $service_id,
                'file_text' => $serviceFile['file_text'],
                'upload_file' => $serviceFile['upload_file'],
            ]);
        }
    }

    public function editService($id)
    {
        $serviceCategories = ServiceCategory::get();
        $service = Service::where('id', $id)->with(['serviceFiles', 'serviceCategories'])->first();
        $selectedServiceCategories = [];
        foreach ($service->serviceCategories as $selectedServiceCategory) {
            $selectedServiceCategories[] = $selectedServiceCategory->id;
        }
        return view('backend.services.create', compact('service', 'serviceCategories', 'selectedServiceCategories'));
    }

    public function updateService(SaveServiceRequest $request)
    {
        $files = $request->file();
        $input = $request->input();
        $service = Service::find($input['id']);

        $image = (isset($files['image'])) ? $this->_uploadImages($files['image'], 'img/backend/services') : $service->image;
        $service->update([
            'title' => $input['title'],
            'title_bg_color' => ($input['title_bg_color']) ? $input['title_bg_color'] : '#002d62',
            'order' => $input['order'],
            'image' => $image,
            'description' => $input['description'],
            'recommended_for' => $input['recommended_for'],
            'cost' => $input['cost'],
            'fee_schedule' => $input['fee_schedule']
        ]);

        $service->serviceCategories()->sync($input['service_category_id']);
        $this->_saveUploadedFiles($request, $service->id);

        return redirect()->route('admin.services')->withFlashSuccess('Service successfully updated.');
    }

    public function deleteService(Request $request)
    {
        $input = $request->input();
        $service = Service::find($input['id']);
        $service->serviceCategories()->detach();
        $service->serviceFiles()->delete();
        $service->delete();
        return $request->session()->flash('flash_success', 'The Service has been successfully deleted.');
    }

    public function editOrder(Request $request)
    {
        $input = $request->input();

        $orders_array = Service::get()->pluck('order')->toArray();
        if (isset($input['service_id'])) {
            $service = Service::where('id', $input['service_id'])->first();
            $current_order = $service->order;
        }

        if (in_array($input['order'], $orders_array)) { //duplicate
            if (!empty($service)) { //existing service
                if ($current_order != $input['order']) {
                    return response()->json(['message' => 'This order is in use!', 'status' => '0'], 200);
                } else {
                    if ($input['form']) {
                        return response()->json(['message' => 'This order number is available.', 'status' => '1'], 200);
                    } else {
                        $service->update(['order' => $input['order']]);
                        return response()->json(['message' => 'The order has been updated.', 'status' => '1'], 200);
                    }
                }
            } else { //new service
                return response()->json(['message' => 'This order is in use!', 'status' => '0'], 200);
            }
        } else { //not duplicate
            if ($input['form']) { 
                return response()->json(['message' => 'This order number is available.', 'status' => '1'], 200);
            } else { //existing service
                $service->update(['order' => $input['order']]);
                return response()->json(['message' => 'The order has been updated.', 'status' => '1'], 200);
            }
        }
    }

    /* SERVICE CATEGORIES */

    public function categories()
    {
        $categories = ServiceCategory::orderBy('id', 'desc')->get();
        return view('backend.services.categories.index', compact('categories'));
    }

    public function createCategory()
    {
        return view('backend.services.categories.create');
    }

    public function saveCategory(SaveServiceCategoryRequest $request)
    {
        $image_name = $this->_handleImage($request->input());
        ServiceCategory::create([
            'name' => $request->name,
            'logo' => $image_name
        ]);
        return redirect()->route('admin.services.categories')->withFlashSuccess('Service Category successfully created.');
    }

    private function _handleImage($input)
    {
        if ($input['logo']['name'] != "") {
            $image_name = date('d-m-y-h-i-s-') . $input['logo']['name'];
            $img = explode(',', $input['logo']['data'])[1];
            $this->_uploadBase64Image(public_path() . '/img/backend/services/categories', $image_name, base64_decode($img));
        } else {
            $image_name = $input['logo_old'];
        }
        return $image_name;
    }

    private function _uploadBase64Image($path, $file, $imgdata)
    {
        $handle = fopen($path . DS . $file, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return TRUE;
    }

    public function editCategory($id)
    {
        $category = ServiceCategory::find($id);
        return view('backend.services.categories.create', compact('category'));
    }

    public function updateCategory(SaveServiceCategoryRequest $request)
    {
        $category = ServiceCategory::find($request->id);
        $image_name = $this->_handleImage($request->input());
        $category->update([
            'name' => $request->name,
            'logo' => $image_name
        ]);
        return redirect()->route('admin.services.categories')->withFlashSuccess('Service Category successfully updated.');
    }

    public function deleteCategory(Request $request)
    {
        $input = $request->input();
        $inUse = $this->_isCategoryInUse($input['id']);
        $totalCategories = ServiceCategory::get()->count();
        $message = ($totalCategories == 1) ? 'You cannot delete all Service Categories.' : 'This Service Category is in use. You cannot delete it.';
        if ($inUse || $totalCategories == 1) {
            return $request->session()->flash('flash_warning', $message);
        } else {
            $category = ServiceCategory::find($input['id']);
            $category->delete();
            return $request->session()->flash('flash_success', 'The Service Category has been successfully deleted.');
        }
    }

    private function _isCategoryInUse($id)
    {
        return ServiceServiceCategory::where('service_category_id', $id)->count();
    }

}
