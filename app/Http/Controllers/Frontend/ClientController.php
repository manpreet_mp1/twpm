<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\StoreClientProfileRequest;
use App\Http\Requests\Frontend\StoreClientDetailRequest;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Profile;
use App\Models\OneTimePassword;
use Response;
use Twilio;
use Carbon\Carbon;
use Session;
Use Auth;
use Cartalyst\Stripe\Stripe;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class ClientController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function signupStepOne()
    {
        return view('frontend.client.stepOne');
    }

    public function signupStepTwo()
    {
        if (Auth::check()) {
            $userId = Auth::id();
        } else {
            $userId = Session::get('userId');
        }
        return view('frontend.client.stepTwo')->with(['userId' => $userId]);
    }

    public function signupStepThree()
    {
        if (Auth::check()) {
            $userId = Auth::id();
        } else {
            $userId = Session::get('userId');
        }
        return view('frontend.client.stepThree')->with(['phone_number' => Profile::where('user_id', $userId)->first(['phone_number']), 'userId' => $userId]);
    }

    public function signupStepFour()
    {
        return view('frontend.client.stepFour');
    }

    public function signupStepFive()
    {
        return view('frontend.client.stepFive');
    }

    public function signupStepSix()
    {
        return view('frontend.client.stepSix');
    }

    /**
     * save user profile details
     * @param StoreClientProfileRequest $request
     * @return type redirect
     */
    public function storeSignupStepTwo(StoreClientProfileRequest $request)
    {
        $profile = new Profile();
        $profile->fill($request->all());
        if ($profile->save()) {
            return redirect()->route('frontend.client.signupStepThree', 'client')->with('userId', $profile->user_id);
        }
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    /**
     * save user profile details which were 
     * @param StoreClientDetailRequest $request
     * @return type
     */
    public function storeSignupStepFour(StoreClientDetailRequest $request)
    {
        $data = $request->all();

        if (Profile::where('user_id', $data['user_id'])->update(['sex' => $data['sex'], 'dob' => $data['dob'], 'ssn' => $data['ssn']])) {
            return redirect()->route('frontend.client.signupStepFive','client')->with('userId', $data['user_id']);
        }
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    /**
     * send otp to user and save otp value in database
     * @param Request $request
     * @return type json
     */
    public function sendOtp(Request $request)
    {
        $data = $request->all();

        if (!isset($data['phone_number']) || empty($data['phone_number']) || $data['phone_number'] < 8) {
            return Response::json(['message' => trans('alerts.frontend.client.phoneRequired')], 500);
        }

        $userId = $data['user_id'];

        if (Profile::where('phone_number', $data['phone_number'])->where('user_id', '!=', $userId)->first()) {
            return Response::json(['message' => trans('alerts.frontend.client.uniquePhone')], 500);
        }

        if (OneTimePassword::where('user_id', $userId)->whereDate('created_at', '=', Carbon::today()->toDateString())->count() > 3) {

            return Response::json(['message' => trans('alerts.frontend.client.otpLimit')], 500);
        } else {

            if ($this->_generateOtp($userId, $data['phone_number'])) {
                return Response::json(['message' => 'Success'], 200);
            } else {
                return Response::json(['message' => trans('alerts.frontend.client.otpFail')], 500);
            }
        }
    }

    /**
     * function to generate otp for phone verification
     * @param type $user
     * @param type $data
     * @param type $otpMessage
     * @return boolean
     */
    private function _generateOtp($userId, $phoneNumber)
    {
        $otp = mt_rand(111111, 999999);

        $one_time_password = new OneTimePassword;
        $one_time_password->user_id = $userId;
        $one_time_password->phone_number = $phoneNumber;
        $one_time_password->otp = $otp;

        $message = trans('alerts.frontend.client.welcomeMsg') . $otp;

        //expiring all previous OTP's active for the user
        OneTimePassword::where('user_id', $userId)->update(['active' => 0]);
        //sending message and saving otp
        if ((Twilio::message('+' . $phoneNumber, $message)) && $one_time_password->save()) {
            return true;
        }
        return false;
    }

    public function verifyOtp(Request $request)
    {
        $data = $request->all();
        $userId = $data['user_id'];

        if (OneTimePassword::where('user_id', $userId)->where('phone_number', $data['phone_number'])->where('otp', $data['otp'])->where('active', 1)->first()) {

            if (Profile::where('user_id', $userId)->update(['phone_verified' => 1]) && OneTimePassword::where('user_id', $userId)->update(['active' => 0])) {
                return Response::json(['message' => 'Success'], 200);
            }
            return Response::json(['message' => trans('alerts.frontend.client.phoneVerifyError')], 500);
        }
        return Response::json(['message' => trans('alerts.frontend.client.invalidOtp')], 500);
    }

    /**
     * create a new customer and make a charge.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function stripePayment(Request $request)
    {

//        $data = $request->all();

        $data = ['card_no' => '4242424242424242', 'exp_month' => '10', 'exp_year' => '2019', 'cvv_no' => '852', 'amount' => '100', 'email' => 'baneet@luminoguruz.com'];
        $stripe = new Stripe(config('services.stripe.secret'));
        try {

            //check if customer already created for given email id then retrieve customer id from db else create customer
            $customer = $stripe->customers()->create([
                'email' => $data['email'],
            ]);

            if (!isset($customer['id'])) {
//                Session::put('error', 'The Stripe Token was not generated correctly');
                dd('The Stripe customer creation failed');
            }

            //save customer id logic here
            //adding new card to stripe dashboard
            $card = $stripe->cards()->create($customer['id'], [
                'number' => $data['card_no'],
                'exp_month' => $data['exp_month'],
                'exp_year' => $data['exp_year'],
                'cvc' => $data['cvv_no'],
            ]);

            if (!isset($card['id'])) {
                dd('failed to add the card');
            }


            $charge = $stripe->charges()->create([
                'customer' => $customer['id'],
                'currency' => 'USD',
                'amount' => $data['amount'],
            ]);
            if ($charge['status'] == 'succeeded') {
                //save  
                dd('money paid success');
            } else {
                dd('fail');
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            dd($e->getMessage());
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            dd($e->getMessage());
        }
    }

}
