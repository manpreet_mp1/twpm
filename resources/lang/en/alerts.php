<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Alert Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain alert messages for various scenarios
      | during CRUD operations. You are free to modify these language lines
      | according to your application's requirements.
      |
     */

    'backend' => [
        'roles' => [
            'created' => 'The role was successfully created.',
            'deleted' => 'The role was successfully deleted.',
            'updated' => 'The role was successfully updated.',
        ],
        'users' => [
            'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
            'created' => 'The user was successfully created.',
            'deleted' => 'The user was successfully deleted.',
            'deleted_permanently' => 'The user was deleted permanently.',
            'restored' => 'The user was successfully restored.',
            'updated' => 'The user was successfully updated.',
            'updated_password' => "The user's password was successfully updated.",
        ]
    ],
    'frontend' => [
        'client' => [
            'saveFail' => 'Oops! Something went wrong. Please try Again!!',
            'realName' => 'Please enter a valid name.',
            'firstName' => 'Please enter your first name.',
            'lastName' => 'Please enter your last name.',
            'shortName' => 'The name must be at least 3 characters.',
            'shortAddress' => 'Too short adress.',
            'addressRequired' => 'Please enter a valid address.',
            'cityRequired' => 'Please enter a valid city.',
            'stateRequired' => 'Please enter a valid state.',
            'zipRequired' => 'Please enter a zip.',
            'zipInvalid' => 'Invalid zip.',
            'phoneRequired' => 'Please enter a phone number.',
            'phoneInavlid' => 'Inavlid phone number.',
            'uniquePhone' => 'Phone number already registered.',
            'phoneTypeRequired' => 'Please choose a phone Type.',
            'sexRequired' => 'Please choose your sex.',
            'dobRequired' => 'Please enter your date of birth. ',
            'dobBefore' => 'You should be 18 or older to signup for twpm.',
            'ssnInavlid' => 'Invalid social security number.',
            'ssnInavlid' => 'Invalid social security number..',
            'ssnRequired' => 'Please eneter your social security number.',
            'ssnUnique' => 'Social security number already registerd.',
            'invalidOtp' => 'Invalid OTP. Please check the number and OTP again.',
            'otpLimit' => 'You have reached max limit to send otp for today. Please try again later.',
            'otpFail' => 'Unable to send OTP. Please try again.',
            'phoneVerifyError' => 'Unable to verify your number. Please try again!!',
            'welcomeMsg' => 'Welocme to TWPM family, your phone verification code is ',
        ],
    ]
];
