$(document).ready(function () {
    var page = 0, can_scroll = true;
    $('#subscribe-newsletter').on('click', function () {
        $('#subscribe-modal').find('.server-response').empty();
        $('#subscribe-modal').modal();
    });
    $('#subscriptionForm,#shareForm').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({ 
            url: $this.attr('action'),
            type: 'POST', 
            data: $this.serialize(),
            success: function (resp) {
                $this.siblings('.server-response').html('<p class="success">' + resp.message + '</p>');
                $this.slideUp();
                setTimeout(function () {
                    $this.slideDown().closest('.modal').modal('hide');
                    $this[0].reset();
                }, 3000);
            },
            error: function (err) {
                $this.siblings('.server-response').html('<p class="error">' + err.message + '</p>');
                $this.slideUp();
                setTimeout(function () {
                    $this.slideDown().closest('.modal').modal('hide');
                    $this[0].reset();
                }, 3000);
            }
        });
    });
    $(window).on('scroll', function () {
        if (($(window).scrollTop() + 100 >= $('#all-articles').offset().top + $('#all-articles').outerHeight() - window.innerHeight) && (can_scroll)) {
            can_scroll = false;
            getNextArticles(page);
            page++;
        }
    });
    function getNextArticles(pageNum) {
        var skip = 8 + (5 * pageNum);
        if ($('#no-more-blogs').length < 1) {
            $.ajax({
                url: getMoreBlogsRoute + '/' + skip,
                processData: false,
                type: "get",
                success: function (resp) {
                    can_scroll = true;
                    var appendlocation = $('#all-articles'),
                            template = $('#single-article-html').html();
                    template = _.template(template);
                    var articles = {
                        articles: resp
                    };
                    appendlocation.append(template(articles));
                    if (resp.length < 5) {
                        $('#all-articles').append('<div id="no-more-blogs"><p>No more blogs found.</p></div>');
                    }
                },
                error: function (err) {
                    alert('Internal Server Error');
                }
            });
        }
    }

});
