@extends('frontend.layouts.master')

@section('title')
Resources &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(elixir('css/resources.css')) }}
@stop

@section('content')
<div class="col-sm-12 blue-bar nav-change"></div>
<div class="over-hidden">
    <div id="resources-outer">
        <div class="col-sm-12 resources" id="all-resources">
            <div class="container">
                <div class="all-resources-box hidden">
                    <?php foreach ($categories as $category) { ?> 
                        <div class="col-sm-3">
                            <div class="services-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            <div class="resources-box" data-categoryname="{{$category->title}}" data-url='{{route('frontend.getResourceTopics',$category->id)}}'>
                                <div class="resources-img" style="background-image: url('img/backend/resources/categories/<?php echo $category->image; ?>')"></div>
                                 <div class="resources-category">
                                    <h3><?php echo (strlen($category->title) > 20) ? substr($category->title, 0, 20) . '...' : $category->title; ?></h3>
                                </div>
                                <button class="btn-white">Details</button>
                            </div>
                        </div> 
                    <?php } ?>
                </div>
                <div class="all-resources-box">
                    <?php foreach ($categories as $category) { ?> 
                        <div class="col-sm-3">
                            <div class="services-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            <div class="resources-box" data-categoryname="{{$category->title}}" data-url='{{route('frontend.getResourceTopics',$category->id)}}'>
                                <div class="resources-img" style="background-color: {{$category->title_bg_color}}">
                                    <h3>{{$category->title}}</h3>
                                </div>
                                 <div class="resources-category">
                                    <h3><?php echo (strlen($category->title) > 20) ? substr($category->title, 0, 20) . '...' : $category->title; ?></h3>
                                </div>
                                <button class="btn-white">Details</button>
                            </div>
                        </div> 
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-sm-12 resources-single-category" id="all-resources-topics"> 
            <div class="container">
                <a class="back-resources">
                    <span class="services-arrow-left"></span>
                    Back to Resources</a>
                <div id='append-resources-topics'>
                </div>
            </div>
        </div>
        <div class="col-sm-12 resources-single-topic" id="single-resources-topic"> 
            <div class="container">
                <a class="back-categories">
                    <span class="services-arrow-left"></span>
                    Back to <span id="categoryName">Categories</span>
                </a>
                <div class="single-topic-content">
                </div>
            </div> 
        </div>
    </div>
</div>
<script id="all-resources-topics-html" type="text/html">
    <%  _.each(topics, function(topic,key){%>
    <div class="col-sm-3">
        <div class="services-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
        <div class="resources-topics hidden" data-id='<%=topic.id%>'>
            <div class="resources-img" style="background-image: url('img/backend/resources/topics/<%=topic.image%>')"></div>
            <div class="resources-category">
                <h3><%=topic.title.length<20?topic.title:topic.title.substring(0, 20)+"..."%></h3> 
            </div>
            <button class="btn-white">Details</button>
            <div class='full-content' style='display: none;'>
                <%=topic.content%>
                <% if(topic.resource_topic_files.length){ %>
                <p class="helpful-links">Helpful Links</p>
                <% } %>
                <ul class="files-list">
                    <%  _.each(topic.resource_topic_files, function(file,key){%>
                    <li><a href="<?php echo URL::asset('img/backend/resources/file_uploads/'); ?>/<%=file.upload_file%>" download="<%=file.upload_file%>" target="_blank"><%=file.file_text%></a></li>
                    <% }) %>
                </ul>
            </div>
        </div>
        <div class="resources-topics" data-id='<%=topic.id%>'>
            <div class="resources-img" style="background-color: <%= topic.title_bg_color %>">
                <h3><%=topic.title %></h3>
            </div>
            <div class="resources-category">
                <h3><%=topic.title.length<20?topic.title:topic.title.substring(0, 20)+"..."%></h3> 
            </div>
            <button class="btn-white">Details</button>
            <div class='full-content' style='display: none;'>
                <%=topic.content%>
                <% if(topic.resource_topic_files.length){ %>
                <p class="helpful-links">Helpful Links</p>
                <% } %>
                <ul class="files-list">
                    <%  _.each(topic.resource_topic_files, function(file,key){%>
                    <li><a href="<?php echo URL::asset('img/backend/resources/file_uploads/'); ?>/<%=file.upload_file%>" download="<%=file.upload_file%>" target="_blank"><%=file.file_text%></a></li>
                    <% }) %>
                </ul>
            </div>
        </div>
    </div> 
    <% }) %>
</script>
@endsection
@section('after-scripts')
{{ Html::script(elixir('js/resources.js')) }}
@stop

