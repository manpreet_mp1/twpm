@extends('frontend.layouts.master')

@section('title')
{{$aboutPageContent['seo_title']}}
@stop

@section('meta_description')
{{$aboutPageContent['seo_description']}} 
@stop

@section('after-styles') 
{{ Html::style(elixir('css/about.css')) }}
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $aboutPageContent['seo_title']; ?>">
<meta itemprop="description" content="<?php echo $aboutPageContent['seo_description']; ?>">
<meta itemprop="image" content="<?php echo URL::asset('img/backend/aboutpage/' . $aboutPageContent['seo_img']); ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@TWPM">
<meta name="twitter:title" content="<?php echo $aboutPageContent['seo_title']; ?>">
<meta name="twitter:description" content="<?php echo $aboutPageContent['seo_description']; ?>">
<meta name="twitter:creator" content="@TWPM">
<meta name="twitter:image" content="<?php echo URL::asset('img/backend/aboutpage/' . $aboutPageContent['seo_img']); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $aboutPageContent['seo_title']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
<meta property="og:image" content="<?php echo URL::asset('img/backend/aboutpage/' . $aboutPageContent['seo_img']); ?>" />
<meta property="og:description" content="<?php echo $aboutPageContent['seo_description']; ?>" /> 
<meta property="og:site_name" content="<?php echo url(''); ?>" />
<meta property="fb:app_id" content="1713884215569225"/> 
@stop

@section('content')
<div class="col-sm-12 about-us">
    <div class="header row">
        <div class='container'>
            <div class="home-heading">{!!$aboutPageContent['slide_1_heading']?$aboutPageContent['slide_1_heading']:''!!}</div>
        </div>
    </div>
    <div class="headline container nav-change"> 
        <div class="headline-heading">{!!$aboutPageContent['slide_2_heading']?$aboutPageContent['slide_2_heading']:''!!}</div>
        <p class='text-center'>{!!$aboutPageContent['slide_2_description']?$aboutPageContent['slide_2_description']:''!!}</p>
    </div>
    <div class="our-mission row">
        <div class="container relative">
            <div class="col-sm-6 static">
                <div class="our-mission-heading">{!!$aboutPageContent['slide_3_heading']?$aboutPageContent['slide_3_heading']:''!!}</div>
            </div>
            <div class="col-sm-6">
                <div class="our-mission-description">{!!$aboutPageContent['slide_3_description']?$aboutPageContent['slide_3_description']:''!!}</div>
            </div>
        </div>
    </div>
    <div class="row over-hidden">
        <div id="team-member-outer">
            <div id="all-team-members">
                <div class="team-list container">
                    <h3 class='text-center'>Meet Your Executive Team</h3>
                    <?php foreach ($employees as $employee) { ?>
                        <div class="col-sm-3 single-employee">
                            <div class='inner-div'>
                                {!! HTML::image('img/backend/aboutpage/employees/'.$employee->image,'Team Member',array('class'=>'img-responsive')) !!}
                                <h4>{{$employee->name}} <span class='pull-right show-employee-details'><i class="fa fa-ellipsis-v" aria-hidden="true"></i></span></h4>
                                <p>{{$employee->title}}</p>
                            </div>
                            <div class='employee-details'>
                                <div class='employee-header'>
                                    <h4>{{$employee->name}}</h4>
                                    <h5>{{$employee->title}}</h5>
                                    <button type="button" class="close hide-employee-details"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class='employee-body'>
                                    <p>
                                        {{strip_tags($employee->description)}}
                                    </p>
                                </div>
                                <div class='employee-footer'>
                                    <div class='col-xs-6 text-center'>
                                        <a class="full-bio-check">Full Bio</a>
                                        <div class="full-employee-details" style="display:none;">
                                            <div class="col-sm-12 full-bio">
                                                <div class="container">
                                                    <a class="back-arrow"><span class="services-arrow-left"></span>Back to Team</a>
                                                    <div class="col-sm-3">
                                                        {!! HTML::image('img/backend/aboutpage/employees/'.$employee->image,'Team Member',array('class'=>'img-responsive')) !!}
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <h1>{{$employee->name}}</h1>
                                                        <h4>{{$employee->title}}</h4>
                                                        <div class="contact-options">
                                                            @if($employee->email)
                                                            <a href="mailto:{{$employee->email}}" target="_top"><i class="fa fa-envelope"></i></a>
                                                            @endif
                                                            @if($employee->phone)
                                                            <a title="tel:{{$employee->phone}}"><i class="fa fa-phone"></i></a>
                                                            @endif
                                                            @if($employee->linkedin)
                                                            <a href="{{$employee->linkedin}}"><i class="fa fa-linkedin"></i></a>
                                                            @endif
                                                            @if($employee->twitter)
                                                            <a href="{{$employee->twitter}}"><i class="fa fa-twitter"></i></a>
                                                            @endif
                                                        </div>
                                                        <div class="description">
                                                            {!! $employee->description  !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-xs-6 text-center contact-modal-show'>
                                        <a href='#'>Contact</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div id="single-team-member" class="full-bio">

            </div>
        </div>
    </div>

    <div class="our-mission row">
        <div class="container relative">
            <div class="col-sm-6 static">
                <div class="our-mission-heading">{!!$aboutPageContent['slide_5_heading']?$aboutPageContent['slide_5_heading']:''!!}</div>
            </div>
            <div class="col-sm-6">
                <div class="our-mission-description">{!!$aboutPageContent['slide_5_description']?$aboutPageContent['slide_5_description']:''!!}</div>
            </div>
        </div>
    </div>
    <div class="questions row">
        <div class="col-sm-6 text-center cta">
            <div class="questions-heading">{!!$footer->heading_subfooter?:''!!}</div>
            <span> {!!$footer->phone?$footer->phone:''!!}</span>
        </div>
        <div class='col-sm-6 text-center'>
            <p>Proud sponsor</p>
            <a href="{{ $aboutPageContent['sponsor_link'] }}" target="_blank">
                {{ HTML::image('img/backend/aboutpage/'.$aboutPageContent['sponsor_logo']) }}
            </a>
        </div>
    </div>

    <hr>
</div>
@endsection

@section('after-scripts')
{{ Html::script(elixir('js/about.js')) }}
@stop