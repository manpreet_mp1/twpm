@extends('frontend.layouts.master')

@section('title')
Services &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(elixir('css/services.css')) }}
@stop

@section('content')
<div class="col-sm-12 blue-bar nav-change"></div>
<div class="col-sm-12 services">
    <div class='col-sm-12' id="servicesOuter">
        <div id='all-services'>
            <div class="col-sm-12 services-nav">
                <ul>
                    <?php foreach ($categories as $category) { ?>
                        <li><a>{{ HTML::image('img/backend/services/categories/'.$category->logo) }} {{$category->name}}</a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="services-lists hidden">
                    <?php foreach ($services as $service) { ?>
                        <div class="col-sm-4">
                            <div class="services-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            <div class="service-box" data-url='{{route('frontend.getServiceDetails',$service->id)}}'>
                                <div class="service-img" style="background-image: url('img/backend/services/{{ $service->image }}')"></div>
                                <div class="service-info">
                                    <h3><?php echo (strlen($service->title) > 35) ? substr($service->title, 0, 35) . '...' : $service->title; ?></h3>
                                    <div class="icons">
                                        <?php foreach ($service->serviceCategories as $erviceCategory) { ?>
                                            {{ HTML::image('img/backend/services/categories/'.$erviceCategory->logo) }}
                                        <?php } ?> 
                                    </div>
                                    <p><?php echo (strlen(strip_tags($service->description)) > 120) ? substr(strip_tags($service->description), 0, 120) . '...' : strip_tags($service->description); ?></p>
                                </div> 
                                <button class="btn-white">Learn More</button>
                            </div>
                        </div> 
                    <?php } ?>
                </div>
                <div class="services-lists">
                    <?php foreach ($services as $service) { ?>
                        <div class="col-sm-4">
                            <div class="services-loader"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
                            <div class="service-box" data-url='{{route('frontend.getServiceDetails',$service->id)}}'>
                                <div class="service-img" style="background-color: {{ $service->title_bg_color }}">
                                    <h3>{{$service->title}}</h3>
                                </div>
                                <div class="service-info">
                                    <h3><?php echo (strlen($service->title) > 35) ? substr($service->title, 0, 35) . '...' : $service->title; ?></h3>
                                    <div class="icons">
                                        <?php foreach ($service->serviceCategories as $erviceCategory) { ?>
                                            {{ HTML::image('img/backend/services/categories/'.$erviceCategory->logo) }}
                                        <?php } ?> 
                                    </div>
                                    <p><?php echo (strlen(strip_tags($service->description)) > 120) ? substr(strip_tags($service->description), 0, 120) . '...' : strip_tags($service->description); ?></p>
                                </div> 
                                <button class="btn-white">Learn More</button>
                            </div>
                        </div> 
                    <?php } ?>
                </div>
            </div>
        </div>
        <div id='single-service'>

        </div>
    </div>
    <div class="container conditions">
        <p>Some services may not be available in all areas.</p>
    </div>
    <div class="col-sm-12 col-xs-12 help-div">
        <div class="container">
            <h2 class="cta"><?php echo strip_tags($footer->heading_subfooter); ?><span>{{ $footer->phone }}</span></h2>
            <h2 class="mobile-cta">{!! $footer->heading_subfooter !!}<span>{{ $footer->phone }}</span></h2>
        </div>
    </div>
</div>
<script id="single-service-html" type="text/html">
    <div class="col-sm-12 service-single"> 
        <div class="container">
            <a class="back-services" id='backToServices'>
                <span class="services-arrow-left"></span>
                Back to Services</a>
            <h1><%=single_service.title%></h1> 
            <div class="col-sm-12 description">
                <div class="col-sm-2 left-heading">Description:</div>
                <div class="col-sm-10 right-desc"><%=single_service.description%></div>
                <div class="clearfix"></div>
                <% if(single_service.recommended_for){ %>
                <div class="col-sm-2 left-heading">Recommended for:</div>
                <div class="col-sm-10 right-desc"><%=single_service.recommended_for%></div>
                <div class="clearfix"></div>
                <% } %>
                <% if(single_service.cost){ %>
                <div class="col-sm-2 left-heading">Cost:</div>
                <div class="col-sm-10 right-desc"> <%=single_service.cost%>
                    <% if(single_service.fee_schedule){ %>
                    <a class="fee-schedule">Fee Schedule</a>
                    <% } %>
                </div>
                <% } %>
            </div>
            <div class="col-sm-12">
                <% if(single_service.service_files.length){ %>
                <p class="helpful-links">Helpful Links</p>
                <% } %>
                <ul class="files-list">
                    <%  _.each(single_service.service_files, function(file,key){%>
                    <li><a href="<?php echo URL::asset('img/backend/services/file_uploads/'); ?>/<%=file.upload_file%>" download="<%=file.upload_file%>" target="_blank"><%=file.file_text%></a></li>
                    <% }) %>
                </ul>
                <div class="start-option">
                    <a class="btn-green get-started">Get Started</a>
                    <p class="help-line hidden">I need help choosing</p>
                </div>
            </div>
        </div> 
    </div>
    <div id="fee-schedule-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <span class="close" data-dismiss="modal">&times;</span>
                <div class="modal-body">
                    <%=single_service.fee_schedule%>
                </div>
            </div>
        </div> 
    </div>
</script>
@endsection

@section('after-scripts')
<script>
    var route = "<?php echo isset($_GET['id']) ? route('frontend.services') . '/' . $_GET['id'] : false; ?>";
</script>
{{ Html::script(elixir('js/services.js')) }}
@stop