@extends('frontend.layouts.master')

@section('title')
{{$homePageContent['seo_title']}}
@stop

@section('meta_description')
{{$homePageContent['seo_description']}}
@stop

@section('after-styles')
{{ Html::style(elixir('css/home.css')) }}
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $homePageContent['seo_title']; ?>">
<meta itemprop="description" content="<?php echo $homePageContent['seo_description']; ?>">
<meta itemprop="image" content="<?php echo URL::asset('img/backend/homepage/' . $homePageContent['seo_img']); ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@TWPM">
<meta name="twitter:title" content="<?php echo $homePageContent['seo_title']; ?>">
<meta name="twitter:description" content="<?php echo $homePageContent['seo_description']; ?>">
<meta name="twitter:creator" content="@TWPM">
<meta name="twitter:image" content="<?php echo URL::asset('img/backend/homepage/' . $homePageContent['seo_img']); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $homePageContent['seo_title']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
<meta property="og:image" content="<?php echo URL::asset('img/backend/homepage/' . $homePageContent['seo_img']); ?>" />
<meta property="og:description" content="<?php echo $homePageContent['seo_description']; ?>" /> 
<meta property="og:site_name" content="<?php echo url(''); ?>" />
<meta property="fb:app_id" content="1713884215569225"/> 
@stop

@section('content')
<?php

use Carbon\Carbon;

if ($homePageContent) {
    $logonumber = sizeof($homePageContent['logoBox']);
    if ($logonumber > 3) {
        $logonumber = 3;
    }
}
?>
<!--slide1-->
<div class="col-sm-12" id="slide1" style="background-image: url('img/backend/homepage/<?php echo $homePageContent['bg_image_1']; ?>')">
    <div class="black-bg">
        <div class="hero-lines">
            <div class="hero-heading"><?php echo $homePageContent['heading_1'] ? $homePageContent['heading_1'] : ''; ?></div>
            <?php echo $homePageContent['subheading_1'] ? '<h3>' . $homePageContent['subheading_1'] . '<h3>' : ''; ?>
        </div>   
        <div class="arrow-down" id="scroll-to-div" data-scrollto="#slide2"></div>
    </div>
</div>

<!--slide2-->
<div class="col-sm-12 nav-change" id="slide2">
    <div class="container">
        <?php
        if ($homePageContent) {
            foreach ($homePageContent['logoBox'] as $logoBoxSingle) {
                ?>
                <div class="col-sm-4 box" style='width:<?php echo 100 / $logonumber; ?>%'>
                    <div class="box-img">
                        {{ HTML::image('img/backend/homepage/logobox/'.$logoBoxSingle['box_logo']) }}
                    </div>
                    <div class="box-title">{{$logoBoxSingle['box_heading']?$logoBoxSingle['box_heading']:''}}</div>
                    <div class="box-description">{{$logoBoxSingle['box_description']?$logoBoxSingle['box_description']:''}}</div>
                </div>  
                <?php
            }
        }
        ?>
    </div>
</div>

<!--slide3-->
<div class="col-sm-12" id="slide3">
    <div class="container">
        <div class='heading-l'><?php echo $homePageContent['heading_3'] ? $homePageContent['heading_3'] : ''; ?></div>
        <?php echo $homePageContent['subheading_3'] ? '<h5>' . $homePageContent['subheading_3'] . '</h5>' : ''; ?>
        <?php foreach ($services as $service) { ?>
            <div class="col-sm-4">
                <div class="service-box" data-url='{{route('frontend.getServiceDetails',$service->id)}}'>
                    <div class="service-img" style="background-image: url('img/backend/services/{{ $service->image }}')"></div>
                    <div class="service-info">
                        <h3>{{ $service->title }}</h3>
                        <div class="icons">
                            <?php foreach ($service->serviceCategories as $serviceCategory) { ?>
                                {{ HTML::image('img/backend/services/categories/'.$serviceCategory->logo) }}
                            <?php } ?>
                        </div>
                        <p><?php echo substr(strip_tags($service->description), 0, 120); ?>...</p>
                    </div>
                    <a class="btn-white" href='{{route('frontend.services')}}?id={{$service->id}}'>Learn More</a>
                </div>
            </div> 
        <?php } ?>
        <a href="{{route('frontend.services')}}" class="see-all">See All</a>
    </div>  
</div>

<!--slide4-->
<div class="col-sm-12" id="slide4">
    <div class="container">
        <div class="col-sm-6 left-div">
            <div class="decades-img" style="background-image: url('img/backend/homepage/<?php echo $homePageContent['image_4']; ?>')"></div>
        </div>
        <div class="col-sm-6">
            <div class="right-div">
                <div class='heading-l'>
                    <?php echo $homePageContent['heading_4'] ? $homePageContent['heading_4'] : ''; ?>
                </div>
                <p>{{$homePageContent['description_4']?$homePageContent['description_4']:''}}</p>
                <a class="meet-team" href="{{route('frontend.about')}}#ourTeam">{{$homePageContent['linktext_4']?$homePageContent['linktext_4']:''}} <span class="arrow-right"></span></a>
            </div>
        </div>
    </div>  
</div>

<!--slide5-->
<div class="col-sm-12" id="slide5">
    <div class="container">
        <h1>Where to begin?</h1>
        <div class="investment-intake">
            <div class="response"></div>
            {{ Form::open(['route'=>'frontend.investmentIntake','id' => 'investment-intake-form']) }}
            <div class="dialogue-1">
                <span>I </span>
                <div class="investment_wrapper">
                    <!--<input type="text" name="consent" class="select-input select-consent"/>-->
                    <span class="select-input select-consent"></span>
                    <ul class="consent select-list" data-input="select-consent">
                        <li data-val="am">am</li>
                        <li data-val="am not">am not</li>
                    </ul>
                </div>
                <span> currently investing. I would consider myself a </span>
                <div class="investment_wrapper">
                    <!--<input type="text" name="level" class="select-input select-level"/>-->
                    <span class="select-input select-level"></span>
                    <ul class="level select-list" data-input="select-level">
                        <li data-val="novice">novice</li>
                        <li data-val="intermediate">intermediate</li>
                        <li data-val="expert">expert</li>
                    </ul>
                </div>
                <span> when it comes to my finances, and I could really use some help with </span>
                <div class="investment_wrapper">
                    <!--<input type="text" name="query_type" class="select-input select-query_type"/>-->
                    <span class="select-input select-query_type"></span>
                    <ul class="query_type select-list" data-input="select-query_type">
                        <li data-val="general questions about investing or financial matters">general questions about investing or financial matters</li>
                        <li data-val="managing my money and debts">managing my money and debts</li>
                        <li data-val="planning for college">planning for college</li>
                        <li data-val="planning for retirement">planning for retirement</li>
                        <li data-val="my 401(k) choices">my 401(k) choices</li>
                        <li data-val="discussing my portfolio">discussing my portfolio</li>
                    </ul>
                </div>
                <span class="btn green_btn proceed">PROCEED</span>
            </div>
            <div class="dialogue-2 ">
                <p>My name is {{ Form::text('name',null,['id'=>'name-input','class' => 'contact-input','placeholder' => 'Name','required']) }}</p>
                <p>You can reach me at {{ Form::text('phone',null,['id'=>'phone-input','class' => 'contact-input','placeholder' => 'Phone','required']) }}</p>
                <p>Or write to me at {{ Form::email('email',null,['id'=>'email-input','class' => 'contact-input','placeholder' => 'Email','required']) }}</p>
                {!! app('captcha')->display(); !!}                
                <small class="contact-response"></small>
                <div class="col-sm-12">
                    <span class="btn white_btn back">BACK</span>
                    {{ Form::submit('HELP ME', ['class' => 'btn green_btn submit_form','disabled']) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<!--slide6-->
<div class="col-sm-12" id="slide6" style="background-image: url('img/backend/homepage/<?php echo $homePageContent['bg_image_6']; ?>')">
    <div class="container">
        <div class="col-sm-6 right-div">
            <h1>{{$homePageContent['heading_6']?$homePageContent['heading_6']:''}}</h1>
            <p>{{$homePageContent['description_6']?$homePageContent['description_6']:''}}</p>
            <a class="discount-service" href="{{route('frontend.services')}}">{{$homePageContent['linktext_6']?$homePageContent['linktext_6']:''}} <span class="flag-icon"></span><span class="arrow-right"></span></a>
        </div>
    </div>
</div>

<!--slide7--> 
<div class="col-sm-12" id="slide7">
    <div class="container">
        <h1>{{$homePageContent['heading_7']?$homePageContent['heading_7']:''}}</h1>
        <?php foreach ($articles as $article) { ?>
            <div class="col-sm-4">
                <a href="{{ route('frontend.single-blog',$article->slug) }}">
                    <div class="commentary-box" style="background-image: url('img/backend/blogs/articles/{{ $article->image }}')">
                        <div class="black-bg">
                            <p>
                                {{Carbon::createFromTimeStamp(strtotime($article->publish_at))->diffForHumans()}} 
                                <?php foreach ($article->blogCategories as $category) { ?>
                                    <span>{{$category->title}}</span>
                                <?php } ?>
                            </p>
                            <h3>{{$article->title}}</h3>
                        </div>
                    </div>  
                </a>
            </div>
        <?php } ?>
    </div>
</div>

<!--slide8-->
<div class="col-sm-12" id="slide8">
    <div class="container">
        <?php if (!empty($footer)) { ?>
            <h2 class="cta"><?php echo strip_tags($footer->heading_subfooter); ?><span>{{ $footer->phone }}</span></h2>
            <h2 class="mobile-cta">{!! $footer->heading_subfooter !!}<span>{{ $footer->phone }}</span></h2>
        <?php } ?>
    </div>
</div>
@endsection

@section('after-scripts')
{{ Html::script(elixir('js/home.js')) }}
@stop