@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')

<div class="client">
    <div id="header">
        <div class="container-fluid account-setup-header">
            <div class="container">
                <div class="header-logo">
                    {{ HTML::image('img/Logo-wht.png') }}
                </div>
            </div>
        </div>
    </div>
    <div id="body-content" class="container-fluid">
        <div class="container">
            <div class="inner-content">
                <h2 class='content-heading' style="color: #030303">Login</h2>

                {{ Form::open(['route' =>'frontend.auth.login','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}

                <div class="form-group" id='basic-information'>
                    {{ Form::label('email', 'Email Address', ['class' => 'col-md-12 control-label','style' => 'text-align: left; color: #2e94e7; padding-left:0px;']) }}
                    <div class='basic-information-content'>
                        {{ Form::text('email', null, ['class' => 'form-control email_address common','required']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>
                    </div>
                    <div class="password-head" style="margin-bottom: 40px;">
                        {{ Form::password('password', ['class' => 'form-control password common', 'placeholder' => 'Password']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('password'); ?></p>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                                </label>
                            </div>
                        </div><!--col-md-6-->
                    </div><!--form-group-->


                    <div class="submit-btn" id='button-color'>
                        {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn contact-info-button']) }}
                    </div>
                    <div class="terms-services">
                        {{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}
                    </div>   
                </div>


            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
@stop