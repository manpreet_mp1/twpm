@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<div class="dashboard">

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                    <div class="col-sm-3 service-outer">
                        <div class="service-name">
                            <p>Product <br>Name</p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
{{ Html::script(elixir('js/dashboard.js')) }}
@stop