<?php
//dump('jjhjj');
?>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(elixir('css/client.css')) }}   
@stop

@section('content')
<?php // dd('asf');   ?>

<div class="client">
    <div id="header">
        <div class="container-fluid account-setup-header">
            <div class="container">
                <div class="header-logo">
                    {{ HTML::image('img/Logo-wht.png') }}
                </div>
                <p>Account Setup</p>
            </div>
        </div>
    </div>

    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 4: Finishing Up</p>
            <div class="col-sm-12 left-bottom-border"></div> 
            <!--<div class="col-sm-4 right-bottom-border"></div>-->
            <div class="inner-content">
                <div id="finishing-up">
                    <h2 class="finishing-content">Setting up your dashboard....</h2>
                </div>

            </div>
            <div class="dashboard">
                <a href="#">DASHBOARD</a>
            </div>
        </div>
    </div>
</div>

@endsection

@section('after-scripts')
@stop