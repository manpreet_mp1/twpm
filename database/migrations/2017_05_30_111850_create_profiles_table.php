<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('first_name', 100);
            $table->string('middle_name', 100)->nullable();
            $table->string('last_name', 100);
            $table->string('first_address');
            $table->string('last_address')->nullable();
            $table->string('city', 100);
            $table->string('state', 100);
            $table->integer('zip');
            $table->string('phone_number',20);
            $table->tinyInteger('phone_type')->default(1)->comment('1 - Mobile, 2 - Home, 3 - Work, 4-Other');
            $table->boolean('phone_verified')->default(0)->comment('0 - Unverified, 1 - Verified');
            $table->tinyInteger('sex')->nullable()->comment('1 - Male, 2 - Female');
            $table->date('dob')->nullable();
            $table->integer('ssn')->nullable()->comment('social security number');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }

}
